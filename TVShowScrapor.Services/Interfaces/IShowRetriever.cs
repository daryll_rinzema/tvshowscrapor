﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TVShowScrapor.Services.Models;

namespace TVShowScrapor.Services.Interfaces
{
    public interface IShowRetriever
    {
        /// <summary>
        /// Gets the maximum of number of pages the API has
        /// </summary>
        /// <returns></returns>
        public int GetMaximumPage();
        /// <summary>
        /// Gets a list of all persons/actors for a given showId
        /// </summary>
        /// <param name="showId"></param>
        /// <returns></returns>
        public IEnumerable<PersonDTO> GetCastForShow(int showId);
        /// <summary>
        /// Gets a list of shows for the given page number
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public IEnumerable<ShowDTO> GetShowsForPage(int page);
    }
}
