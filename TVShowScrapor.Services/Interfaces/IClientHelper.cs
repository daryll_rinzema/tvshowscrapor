﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TVShowScrapor.Services.Interfaces
{
    public interface IClientHelper
    {
        /// <summary>
        /// Checks of the request to the given resource gives a successful response
        /// </summary>
        /// <param name="apiResource"></param>
        /// <returns></returns>
        bool SuccessExecute(string apiResource);

        /// <summary>
        /// Execute an api call towards the given RestClient for the given Type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="apiResource"></param>
        /// <returns></returns>
        Task<T> ExecuteApiCall<T>(string apiResource);

        /// <summary>
        /// Execute an api call towards the given RestClient for the given Type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="apiResource"></param>
        /// <returns></returns>
        Task<T> ExecuteApiCall<T>(string apiResource, Method method);
    }
}
