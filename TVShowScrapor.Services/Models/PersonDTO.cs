﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TVShowScrapor.Services.Models
{
    public class PersonDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public DateTime Birthday { get; set; }

        //more required properties here....
    }
}
