﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TVShowScrapor.Services.Models
{
    public class ShowDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<PersonDTO> Cast { get; set; }

        //more required properties here....
    }
}
