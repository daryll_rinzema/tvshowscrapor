﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using TVShowScrapor.Services.Interfaces;

namespace TVShowScrapor.Services.Implementations
{
    public class TVMazeClientHelper: IClientHelper
    {
        private readonly IRestClient _client;

        public TVMazeClientHelper(string baseUrl)
        {
            _client = new RestClient(baseUrl);
        }

        public bool SuccessExecute(string apiResource)
        {
            var request = new RestRequest(apiResource, Method.GET);

            var response = _client.Execute(request);

            return response.IsSuccessful;
        }

        public async Task<T> ExecuteApiCall<T>(string apiResource)
        {
            var request = new RestRequest(apiResource, Method.GET);

            var response = await _client.ExecuteAsync<object>(request);

            if (response.IsSuccessful)
            {
                return JsonSerializer.Deserialize<T>(response.Content);
            }

            throw new Exception($"Error while executing API call. {response.ErrorMessage} {response.Content}", response.ErrorException);
        }

        public Task<T> ExecuteApiCall<T>(string apiResource, Method method)
        {
            throw new NotImplementedException();
        }
    }
}
