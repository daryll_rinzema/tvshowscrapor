﻿using RestSharp;
using System.Collections.Generic;
using TVShowScrapor.Services.Interfaces;
using TVShowScrapor.Services.Models;
using System;
using System.Threading.Tasks;
using System.Text.Json;
using TVShowScrapor.Services.Models.TVMAZE;
using TVShowScrapor.Services.Models.TVMAZE.SHOW;
using TVShowScrapor.Services.Models.TVMAZE.PERSON;

namespace TVShowScrapor.Services.Implementations
{
    public class TVMazeShowRetriever : IShowRetriever
    {
        private readonly IClientHelper _clientHelper;

        public TVMazeShowRetriever(IClientHelper clientHelper)
        {
            _clientHelper = clientHelper;
        }

        public int GetMaximumPage()
        {
            bool checking = true;
            int page = 0;

            //there seems to be no indication in the TVMAZE api to know how many shows there are
            //and the total amount of pages... so we use a loop to discover this
            while (checking)
            {
                var apiResource = $"/shows?page={page}";

                var success = _clientHelper.SuccessExecute(apiResource);
                if (success)
                {
                    // we up the page to check for next request + results
                    page++;
                } else
                {
                    checking = false;
                }


            }

            return page;
        }

        public IEnumerable<ShowDTO> GetShowsForPage(int page)
        {
            var DTOs = new List<ShowDTO>();

            var apiResourcePage = $"/shows?page={page}";

            try
            {
                //JSON Deserialize is a big class, find a way to only get selected properties...
                var pageResult = _clientHelper.ExecuteApiCall<List<SHOW>>(apiResourcePage).Result;
                foreach (var tvmazeShow in pageResult)
                {
                    var showDTO = new ShowDTO()
                    {
                        Id = tvmazeShow.id,
                        Name = tvmazeShow.name,
                        Cast = new List<PersonDTO>()
                    };

                    var apiResourceShow = $"/shows/{showDTO.Id}/cast";
                    var castResult = _clientHelper.ExecuteApiCall<List<PERSON>>(apiResourceShow).Result;

                    foreach(var person in castResult)
                    {
                        //resolve better way of nullable birthday, now value = minimum
                        DateTime.TryParse(person.person.birthday, out DateTime birthday);

                        var personDTO = new PersonDTO()
                        {
                            Id = person.person.id,
                            Name = person.person.name,
                            Birthday = birthday
                        };

                        showDTO.Cast.Add(personDTO);
                    }

                    DTOs.Add(showDTO);
                }
            }
            catch (Exception e)
            {
                //log exception
            }

            return DTOs;
        }

        public IEnumerable<PersonDTO> GetCastForShow(int showId)
        {
            throw new NotImplementedException();
        }
    }
}
