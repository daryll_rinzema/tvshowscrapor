using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestSharp;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TVShowScrapor.Services.Implementations;
using TVShowScrapor.Services.Interfaces;
using TVShowScrapor.Services.Models.TVMAZE.PERSON;
using TVShowScrapor.Services.Models.TVMAZE.SHOW;

namespace TVShowScrapor.UnitTests
{
    [TestClass]
    public class TVMazeShowRetrieverTests
    {
        private TVMazeShowRetriever _showRetriever;

        [TestInitialize]
        public void Initialize()
        {
            var getter = new JsonResultGetter();
            var pageresult = getter.GetDeserializedJson<List<SHOW>>("pageresult");
            var showcastresult = getter.GetDeserializedJson<List<PERSON>>("castresult");

            var clientHelper = new Mock<IClientHelper>();
            clientHelper.Setup(c => c.SuccessExecute(It.IsAny<string>())).Returns(true);
            clientHelper.Setup(c => c.ExecuteApiCall<List<SHOW>>(It.IsAny<string>()))
                .Returns(Task.FromResult(pageresult));
            clientHelper.Setup(c => c.ExecuteApiCall<List<PERSON>>(It.IsAny<string>()))
                .Returns(Task.FromResult(showcastresult));
            _showRetriever = new TVMazeShowRetriever(clientHelper.Object);
        }

        [TestMethod]
        public void GivenATVMAZEShow_WithAllFieldsSet_ShoudReturnShowDTOWithCast()
        {
            //Arrange
            //we wrapped a mock object around the RestSharp client to return dummydata
            //mock results of page and cast per show

            //Act
            var result = _showRetriever.GetShowsForPage(0);
            result = result.ToList();

            //Assert
            //check here for full DTO

            result.Should().NotBeEmpty();
            result.First().Cast.Should().HaveCount(15);
            result.First().Name.Equals("Under the Dome");
            //etc...
        }

        // implement more unit test with distorted data
    }
}
