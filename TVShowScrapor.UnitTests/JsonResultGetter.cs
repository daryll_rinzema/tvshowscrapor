﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace TVShowScrapor.UnitTests
{
    public class JsonResultGetter
    {
        public T GetDeserializedJson<T>(string filename)
        {
            var jsonData = File.ReadAllText(@$"{filename}.json");
            return JsonSerializer.Deserialize<T>(jsonData);
        }
}
}
