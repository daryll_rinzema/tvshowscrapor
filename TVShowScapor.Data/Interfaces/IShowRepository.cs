﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TVShowScapor.Data.Entities;

namespace TVShowScapor.Data.Interfaces
{
    public interface IShowRepository
    {
        Show Get(int id);
        Task<IEnumerable<Show>> GetAll(int top = 1000);
        Task Create(Show entity);
        Task Create(IEnumerable<Show> entities);
        Task Update(Show entity);
        Task Delete(Show entity);
        Task DeleteAsync(IEnumerable<Show> entities);
    }
}
