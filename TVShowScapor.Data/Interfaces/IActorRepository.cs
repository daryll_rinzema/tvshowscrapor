﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TVShowScapor.Data.Entities;

namespace TVShowScapor.Data.Interfaces
{
    public interface IActorRepository
    {
        Actor Get(int id);
        IEnumerable<Actor> GetAll();
        Task Create(Actor entity);
        Task Create(IEnumerable<Actor> entities);
        Task Update(Actor entity);
        Task Delete(Actor entity);
        Task DeleteAsync(IEnumerable<Actor> entities);
    }
}
