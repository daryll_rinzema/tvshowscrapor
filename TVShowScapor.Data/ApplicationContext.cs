﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TVShowScapor.Data.Entities;

namespace TVShowScapor.Data
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }
        public DbSet<Show> Shows { get; set; }
        public DbSet<Actor> Actors { get; set; }
    }
}
