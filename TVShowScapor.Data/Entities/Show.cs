﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TVShowScapor.Data.Entities
{
    public class Show
    {
        public Show()
        {
            this.Cast = new HashSet<Actor>();
        }

        [Key]
        public int Id { get; set; }

        public int ExternalId { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Actor> Cast { get; set; }
    }
}
