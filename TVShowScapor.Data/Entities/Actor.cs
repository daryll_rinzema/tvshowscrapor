﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TVShowScapor.Data.Entities
{
    public class Actor
    {
        public Actor()
        {
            //many-to-many
            this.Shows = new HashSet<Show>();
        }

        [Key]
        public int Id { get; set; }

        public int ExternalId { get; set; }
        public string Name { get; set; }
        public DateTime Birthday { get; set; }

        public virtual ICollection<Show> Shows { get; set; }
    }
}
