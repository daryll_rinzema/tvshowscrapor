﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVShowScapor.Data.Entities;
using TVShowScapor.Data.Interfaces;

namespace TVShowScapor.Data.Repositories
{
    public class ActorRepository : IActorRepository
    {
        private readonly ApplicationContext _context;

        public ActorRepository(ApplicationContext context)
        {
            _context = context;
        }

        public Actor Get(int id)
        {
            var entity = _context.Actors.FirstOrDefault(e => e.Id == id);
            return entity;
        }

        public IEnumerable<Actor> GetAll()
        {
            var entities = _context.Actors;
            return entities;
        }

        public async Task Create(Actor entity)
        {
            _context.Actors.Add(entity);

            await _context.SaveChangesAsync();
        }

        public async Task Create(IEnumerable<Actor> entities)
        {
            _context.Actors.AddRange(entities);

            await _context.SaveChangesAsync();
        }

        public async Task Delete(Actor entity)
        {
            _context.Actors.Remove(entity);

            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(IEnumerable<Actor> entities)
        {
            _context.Actors.RemoveRange(entities);

            await _context.SaveChangesAsync();
        }

        public async Task Update(Actor entity)
        {
            var dbEntity = this.Get(entity.Id);
            if(dbEntity != null)
            {
                dbEntity.Name = entity.Name;
                dbEntity.Birthday = entity.Birthday;
            }

            await _context.SaveChangesAsync();
        }
    }
}
