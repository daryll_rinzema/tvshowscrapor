﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVShowScapor.Data.Entities;
using TVShowScapor.Data.Interfaces;

namespace TVShowScapor.Data.Repositories
{
    public class ShowRepository : IShowRepository
    {
        private readonly ApplicationContext _context;

        public ShowRepository(ApplicationContext context)
        {
            _context = context;
        }

        public Show Get(int id)
        {
            var entity = _context.Shows.Include(s => s.Cast).FirstOrDefault(e => e.Id == id);

            return entity;
        }

        public async Task<IEnumerable<Show>> GetAll(int top = 1000)
        {
            //automaically load cast too
            var entities = await _context.Shows.Take(top).Include(s => s.Cast).ToListAsync();

            return entities;
        }

        public async Task Create(Show entity)
        {
            _context.Shows.Add(entity);

            await _context.SaveChangesAsync();
        }

        public async Task Create(IEnumerable<Show> entities)
        {
            _context.Shows.AddRange(entities);

            await _context.SaveChangesAsync();
        }

        public async Task Delete(Show entity)
        {
            _context.Shows.Remove(entity);

            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(IEnumerable<Show> entities)
        {
            _context.Shows.RemoveRange(entities);

            await _context.SaveChangesAsync();
        }

        public async Task Update(Show entity)
        {
            var dbEntity = this.Get(entity.Id);
            if (dbEntity != null)
            {
                dbEntity.Name = entity.Name;
                dbEntity.Cast = entity.Cast;
            }

            await _context.SaveChangesAsync();
        }
    }
}
