﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TVShowScapor.Data.Entities;
using TVShowScapor.Data.Interfaces;
using TVShowScrapor.Services.Interfaces;
using TVShowScrapor.Services.Models;

namespace TVShowScrapor.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/[controller]")]
    public class ShowController : ControllerBase
    {
        private readonly ILogger<ShowController> _logger;

        private readonly IShowRetriever _showRetriever;
        private readonly IShowRepository _showRepository;

        //put mapping inside own service
        private readonly IMapper _mapper;

        public ShowController(ILogger<ShowController> logger,
            IShowRetriever showRetriever,
            IShowRepository showRepository,
            IMapper mapper)
        {
            _logger = logger;

            _showRetriever = showRetriever;
            _showRepository = showRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<IEnumerable<ShowDTO>> GetShows(int top = 1000)
        {
            var shows = await _showRepository.GetAll(top);
            shows = shows.ToList();

            var showDTOs =_mapper.Map<IEnumerable<ShowDTO>>(shows);

            return showDTOs;
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<IActionResult> SynchronizeShows()
        {
            var maxPage = _showRetriever.GetMaximumPage();

            //when doing per page and inserting in "batches" no error from server about too many requests
            for(var i = 0; i < maxPage; i++)
            {
                var showDTOs = _showRetriever.GetShowsForPage(i);
                //expected problem of already tracked actors is here....
                //no more time to truly synchronize...
                var showsEntities = _mapper.Map<IEnumerable<Show>>(showDTOs); //also includes actors
                await _showRepository.Create(showsEntities);
            }

            //now only inserts...
            //implement methods to synchronize / update existing ones

            return Ok();
        }
    }
}
