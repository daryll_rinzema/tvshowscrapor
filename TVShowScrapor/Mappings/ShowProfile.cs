﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TVShowScapor.Data.Entities;
using TVShowScrapor.Services.Models;

namespace TVShowScrapor.API.Mappings
{
    public class ShowProfile : Profile
    {
        public ShowProfile()
        {
            CreateMap<PersonDTO, Actor>()
                .ForMember(t => t.ExternalId, opt => opt.MapFrom(s => s.Id))
                .ForMember(t => t.Name, opt => opt.MapFrom(s => s.Name))
                .ForMember(t => t.Birthday, opt => opt.MapFrom(s => s.Birthday))
                .ReverseMap()
                .ForAllOtherMembers(t => t.Ignore());

            CreateMap<ShowDTO, Show>()
                .ForMember(t => t.ExternalId, opt => opt.MapFrom(s => s.Id))
                .ForMember(t => t.Name, opt => opt.MapFrom(s => s.Name))
                .ForMember(t => t.Cast, opt => opt.MapFrom(s => s.Cast))
                .ReverseMap()
                .ForAllOtherMembers(t => t.Ignore());
        }
    }
}
